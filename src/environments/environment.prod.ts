export const environment = {
  production: true,
  apiUrl: 'apiUrl',
  GOOGLE_API_KEY: 'GOOGLE_API_KEY',
  GOOGLE_AUTO_COMPLETE_URL:'GOOGLE_AUTO_COMPLETE_URL'
};
