import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoginRequest } from 'src/app/shared/interfaces';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() {}

  login(loginRequest: LoginRequest) {
    return of();
  }
}
