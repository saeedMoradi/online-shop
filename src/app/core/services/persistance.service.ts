import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersistanceService {
  set(key: string, data: any) {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch (e) {
      console.error('Error saving to localstorage\n', e);
    }
  }
  get(key: string) {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      console.error('Error getting data from localstorage\n', e);
      return null;
    }
  }
  remove(key: string) {
    try {
      return localStorage.removeItem(key);
    } catch (e) {
      console.error('Error removing data from localstorage\n', e);
      return null;
    }
  }
}
