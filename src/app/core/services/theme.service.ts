import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { PersistanceService } from './persistance.service';

enum Theme {
  Light = 'light',
  Dark = 'dark'
}

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private persistanceService: PersistanceService
  ) {}

  switchTheme(className: string): void {
    if (!this.targetElement.classList.contains(className)) {
      this.removeAllClasses();
      this.addClass('theme-transition');
      this.addClass(className);
      this.persistanceService.set('theme', className);
      setTimeout(() => {
        this.removeClass('theme-transition');
      }, 1500);
    }
  }

  setTheme(className = Theme.Light): void {
    const theme = this.persistanceService.get('theme');
    if (theme) {
      if (Object.values(Theme).includes(theme)) {
        this.addClass(theme);
      } else {
        this.persistanceService.remove('theme');
        this.addClass(className);
      }
    } else {
      this.addClass(className);
    }
  }

  get getTheme(): string {
    return this.persistanceService.get('theme');
  }

  private removeAllClasses(): void {
    this.targetElement.className = '';
  }

  private removeClass(arr: string): void {
    this.targetElement.classList.remove(arr);
  }

  private addClass(arr: string): void {
    this.targetElement.classList.add(arr);
  }

  private get targetElement(): HTMLElement {
    let elem = this.document.documentElement;
    return elem;
  }
}
