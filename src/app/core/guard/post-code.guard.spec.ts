import { TestBed } from '@angular/core/testing';

import { PostCodeGuard } from './post-code.guard';

describe('PostCodeGuard', () => {
  let guard: PostCodeGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PostCodeGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
