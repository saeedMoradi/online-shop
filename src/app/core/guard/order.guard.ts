import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap, switchMap, filter } from 'rxjs/operators';
import { getShopData } from 'src/app/features/home/+store/shop.actions';
import { selectMerchantShopMenu } from 'src/app/features/home/+store/shop.selector';

@Injectable({
  providedIn: 'root'
})
export class OrderGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const { merchant, shop } = route.params;
    return this.store.select(selectMerchantShopMenu).pipe(
      tap((data: any) => {
        if (!data.shop) {
          this.store.dispatch(
            getShopData({
              slug: shop,
              merchantSlug: merchant,
              postCode: '',
              serviceTypeId: null,
              guard: true
            })
          );
        }
      }),
      filter((data: any) => {
        return (
          typeof data.shop === 'string' ||
          (data.shop !== null && data.menu !== null)
        );
      }),
      switchMap((data: any) => {
        // TODO: handle shop error
        if (typeof data.shop === 'string') {
          // TODO: craete shop 404 and navigate to it
          this.router.navigate(['/404']);
          return of(false);
        } else {
          return of(true);
        }
      })
    );
  }
}
