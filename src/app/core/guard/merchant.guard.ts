import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap, filter, switchMap } from 'rxjs/operators';
import { getMerchant } from 'src/app/features/home/+store/shop.actions';
import { selectActiveMerchant } from 'src/app/features/home/+store/shop.selector';

@Injectable({
  providedIn: 'root'
})
export class MerchantGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const { merchant } = route.params;
    return this.store.select(selectActiveMerchant).pipe(
      tap((data: any) => {
        if (data === null) {
          this.store.dispatch(getMerchant({ slug: merchant }));
        }
      }),
      filter((data: any) => {
        return data !== null;
      }),
      switchMap((data: any) => {
        if (typeof data === 'string') {
          this.router.navigate(['/404']);
          return of(false);
        } else {
          return of(true);
        }
      })
    );
  }
}
