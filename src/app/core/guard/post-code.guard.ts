import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { selectPostCode } from '../../features/home/+store/shop.selector';

@Injectable({
  providedIn: 'root'
})
export class PostCodeGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.store.select(selectPostCode).pipe(
      take(1),
      switchMap((data: string | null) => {
        if (data) {
          return of(true);
        }
        this.router.navigate(['/']);
        return of(false);
      })
    );
  }
}
