import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthModule } from 'src/app/features/auth/auth.module';
// 3rd party Modules
import { DialogModule } from '@ngneat/dialog';
import { PlatformModule } from '@angular/cdk/platform';
import { LayoutModule } from '@angular/cdk/layout';
// Components
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { FooterComponent } from './layout/footer/footer.component';
// NGRX
import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
// Store
import { ROOT_REDUCERS } from './+store';
import { ShopEffects } from '../features/home/+store/shop.effects';
import { HomeService } from '../features/home/services/home.service';

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  declarations: [MainLayoutComponent, NavbarComponent, FooterComponent],
  imports: [
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    PlatformModule,
    LayoutModule,
    DialogModule.forRoot(),
    StoreModule.forRoot(ROOT_REDUCERS, {}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([ShopEffects]),
    StoreRouterConnectingModule.forRoot(),
    AuthModule
  ],
  providers: [HomeService],
  exports: [MainLayoutComponent]
})
export class CoreModule {}
