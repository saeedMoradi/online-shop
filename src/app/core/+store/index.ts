import { Action, ActionReducerMap } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { InjectionToken } from '@angular/core';
import { orderReducer, OrderState } from 'src/app/features/order/+store';
import { shopReducer, ShopState } from 'src/app/features/home/+store';

export interface State {
  router: fromRouter.RouterReducerState<any>;
  [orderReducer.featureKey]: OrderState;
  [shopReducer.featureKey]: ShopState;
}

export const ROOT_REDUCERS = new InjectionToken<
  ActionReducerMap<State, Action>
>('Root reducers token', {
  factory: () => ({
    router: fromRouter.routerReducer,
    [orderReducer.featureKey]: orderReducer.reducer,
    [shopReducer.featureKey]: shopReducer.reducer
  })
});
