import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  NgZone,
  AfterViewInit,
  OnDestroy,
  Inject,
  ChangeDetectorRef
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AuthFacade } from 'src/app/features/auth/+store/auth.facade';
import {
  selectBasketTotal,
  selectBasketTotalItems
} from 'src/app/features/order/+store/order.selectors';
import { logout } from 'src/app/features/auth/+store/auth.actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit, AfterViewInit, OnDestroy {
  isMenuOpen = false;
  total$: Observable<any> = this.store.select(selectBasketTotal);
  totalItem$: Observable<any> = this.store.select(selectBasketTotalItems);
  destroy$ = new Subject();

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private zone: NgZone,
    private cd: ChangeDetectorRef,
    public authFacade: AuthFacade,
    private store: Store<any>
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      var el = document.querySelector('.slide-menu');
      fromEvent(this.document, 'click')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event: Event) => {
          if (this.isMenuOpen) {
            if (!el.contains(event.target as Node)) {
              this.changeMenu();
              this.cd.detectChanges();
            }
          }
        });
    });
  }

  changeMenu() {
    event.stopPropagation();
    this.isMenuOpen = !this.isMenuOpen;
  }

  logout() {
    this.authFacade.dispatch(logout());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
