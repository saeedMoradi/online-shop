import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { MerchantGuard } from './core/guard/merchant.guard';
import { OrderGuard } from './core/guard/order.guard';
// import { PostCodeGuard } from './core/guard/post-code.guard';
import { AuthGuard } from './features/auth/guards/auth.guard';

const routes: Routes = [
  
  // {
  //   path: 'order-now',
  //   loadChildren: () =>
  //     import('./features/order/order.module').then(m => m.OrderModule),
  //   canActivate: [PostCodeGuard]
  // },
  {
    path: 'reviews',
    loadChildren: () =>
      import('./features/reviews/reviews.module').then(m => m.ReviewsModule)
  },
  {
    path: 'contact-us',
    loadChildren: () =>
      import('./features/contact-us/contact-us.module').then(
        m => m.ContactUsModule
      )
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./features/manual-auth/manual-auth.module').then(
        m => m.ManualAuthModule
      )
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./features/profile/profile.module').then(m => m.ProfileModule),
    canActivate: [AuthGuard]
  },
  {
    path: ':merchant',
    pathMatch: 'full',
    loadChildren: () =>
      import('./features/home/home.module').then(m => m.HomeModule),
    canActivate: [MerchantGuard]
  },
  {
    path: ':merchant/:shop',
    loadChildren: () =>
      import('./features/order/order.module').then(m => m.OrderModule),
    canActivate: [OrderGuard]
  },
  {
    path: '404',
    loadChildren: () =>
      import('./features/not-found/not-found.module').then(
        m => m.NotFoundModule
      )
  },
  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
      relativeLinkResolution: 'legacy'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
