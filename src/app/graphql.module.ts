import { NgModule } from '@angular/core';
import { APOLLO_OPTIONS } from 'apollo-angular';
import {
  ApolloClientOptions,
  ApolloLink,
  InMemoryCache
} from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
// import { setContext } from '@apollo/client/link/context';

const uri = 'http://188.40.181.213:8080/api/graphql';
export function createApollo(httpLink: HttpLink): ApolloClientOptions<any> {
  // const basic = setContext((operation, context) => ({
  //   headers: {
  //     Accept: 'charset=utf-8'
  //   }
  // }));
  // const token = localStorage.getItem('token');
  // const auth = setContext((operation, context) => ({
  //   headers: {
  //     Authorization: `Bearer ${token}`
  //   }
  // }));
  const link = ApolloLink.from([httpLink.create({ uri })]);
  const cache = new InMemoryCache();

  return {
    link,
    cache
  };
}

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink]
    }
  ]
})
export class GraphQLModule {}
