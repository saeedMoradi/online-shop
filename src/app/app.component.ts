import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AuthFacade } from './features/auth/+store/auth.facade';
import { checkAuth } from './features/auth/+store/auth.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  constructor(private authFacade: AuthFacade) {
    this.authFacade.dispatch(checkAuth());
  }
}
