import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  EventEmitter,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-delivery-address',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeliveryAddressComponent implements OnInit {
  addressForm: FormGroup;
  createMode = false;
  @Output() onAddress = new EventEmitter();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initializeAddressForm();
  }

  initializeAddressForm() {
    this.addressForm = this.fb.group({
      address: [''],
      user: ['']
    });
  }

  getAddress(value) {
    this.onAddress.emit(value);
  }

  addAddress() {
    this.createMode = !this.createMode;
  }
  cancel() {
    this.createMode = !this.createMode;
  }
  add() {
    this.createMode = !this.createMode;
  }
}
