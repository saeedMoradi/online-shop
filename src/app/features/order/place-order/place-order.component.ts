import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  AfterViewInit,
  ElementRef,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { createBasketOrder } from '../+store/order.actions';
import { selectSubmitedBasketDetails } from '../+store/order.selectors';
import { loadStripe, Stripe } from '@stripe/stripe-js';
import {
  selectBasket,
  selectBasketTotal,
  selectBasketAndTotal
} from '../+store/order.selectors';
import {
  changeBasketQuantity,
  createShoppingBasket
} from '../+store/order.actions';
import {
  selectActiveShop,
  selectShopCurrency
} from '../../home/+store/shop.selector';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaceOrderComponent implements OnInit, OnDestroy, AfterViewInit {
  // @ViewChild('cardElement') cardElement: ElementRef;
  intentSecretCode: string;
  stripe: Stripe;
  card;
  basket;
  basketTotal$: Observable<any> = this.store.select(selectBasketTotal);
  currencySymbol$: Observable<any> = this.store.select(selectShopCurrency);
  cardErrors;
  loading = false;
  errors: {
    stripeError: string;
    intentError: string;
  } = {
    stripeError: '',
    intentError: ''
  };
  mode: string = 'Delivery';
  address: string = 'Hove';
  destroy$ = new Subject();

  constructor(private store: Store, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.store
    .select(selectBasket)
    .pipe(takeUntil(this.destroy$))
    .subscribe((data: any) => {
      this.basket = data;
      this.cd.detectChanges();
    });
  }

  ngAfterViewInit(): void {
    this.initializeStripe();
  }

  async initializeStripe() {
    const stripeKey = 'pk_test_TYooMQauvdEDq54NiTphI7jx';
    this.stripe = await loadStripe(stripeKey);
    const elements = this.stripe.elements();
    this.card = elements.create('card', {
      style: {
        base: {
          backgroundColor: '#ffffff'
        }
      }
    });
    // this.card.mount(this.cardElement.nativeElement);
    // this.card.addEventListener('change', ({ error }) => {
    //   this.cardErrors = error && error.message;
    //   this.cd.detectChanges();
    // });
  }

  onChangeMode($event: string) {
    this.mode = $event;
  }

  onAddress(e) {
    this.address = e;
  }

  pay() {
    // if (this.merchantId) {
    //   this.loading = true;
    //   if (this.intentSecretCode && this.enteredVoucher) {
    //     this.stripeConfirmPayment(this.intentSecretCode, this.enteredVoucher);
    //   } else {
    //     this.becomeAPartnerService
    //       .intent('GBP', '29500')
    //       .pipe(takeUntil(this.destroy))
    //       .subscribe(
    //         (data: Intent) => {
    //           this.errors = { ...this.errors, intentError: '' };
    //           this.stripeConfirmPayment(data.client_secret, '');
    //         },
    //         (error) => {
    //           this.errors.intentError = error;
    //           this.loading = false;
    //           this.cd.detectChanges();
    //         }
    //       );
    //   }
    // } else {
    //   this.snackBar.open('Merchant Not Found, Please Login First');
    // }
  }

  // async stripeConfirmPayment(intentSecretCode, voucher) {
  //   const { paymentIntent, error } = await this.stripe.confirmCardPayment(
  //     intentSecretCode,
  //     {
  //       payment_method: {
  //         card: this.card,
  //       },
  //     }
  //   );
  //   if (error) {
  //     this.cardErrors = error.message;
  //     this.loading = false;
  //     this.cd.detectChanges();
  //   } else if (paymentIntent) {
  //     const params = {
  //       merchantId: this.merchantId,
  //       voucher,
  //       stripe: paymentIntent,
  //     };
  //     this.becomeAPartnerService
  //       .submit(params)
  //       .pipe(takeUntil(this.destroy))
  //       .subscribe(
  //         (data) => {
  //           this.router.navigateByUrl('/login');
  //         },
  //         (error) => {
  //           this.errors = { ...this.errors, stripeError: error };
  //           this.loading = false;
  //           this.cd.detectChanges();
  //         }
  //       );
  //   }
  // }

  orderComplete() {
    if (!this.address) {
      return;
    }
    this.store
      .pipe(select(selectSubmitedBasketDetails), takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.store.dispatch(
            createBasketOrder({
              request: {
                shoppingBasket: data['@id'],
                address: this.address
              }
            })
          );
        }
      });
  }

  add(item) {
    this.store.dispatch(changeBasketQuantity({ item, quantity: +1 }));
  }

  remove(item) {
    this.store.dispatch(changeBasketQuantity({ item, quantity: -1 }));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
