import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CartModule } from 'src/app/features/order/cart/cart.module';
import { PlaceOrderRoutingModule } from './place-order-routing.module';
import { PlaceOrderComponent } from './place-order.component';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { PaymentCardsComponent } from './payment-cards/payment-cards.component';
import { PickupTimeComponent } from './pickup-time/pickup-time.component';
import { PromotionalCodeComponent } from './promotional-code/promotional-code.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PlaceOrderComponent,
    DeliveryAddressComponent,
    PaymentCardsComponent,
    PickupTimeComponent,
    PromotionalCodeComponent
  ],
  imports: [
    SharedModule,
    CartModule,
    PlaceOrderRoutingModule,
    ReactiveFormsModule
  ]
})
export class PlaceOrderModule {}
