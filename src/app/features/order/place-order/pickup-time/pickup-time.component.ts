import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-pickup-time',
  templateUrl: './pickup-time.component.html',
  styleUrls: ['./pickup-time.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PickupTimeComponent implements OnInit {

  showDel: boolean = false;
  showPick: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  selectCheck(value) {
    if (value === 'delivery') {
      this.showDel = true;
      this.showPick = false;
    }else {
      this.showPick = true;
      this.showDel = false;
    }
  }

}
