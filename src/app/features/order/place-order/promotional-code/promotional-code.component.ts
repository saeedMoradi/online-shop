import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-promotional-code',
  templateUrl: './promotional-code.component.html',
  styleUrls: ['./promotional-code.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PromotionalCodeComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  addPromo() {}
}
