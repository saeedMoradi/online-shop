import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PromotionalCodeComponent } from './promotional-code.component';

describe('PromotionalCodeComponent', () => {
  let component: PromotionalCodeComponent;
  let fixture: ComponentFixture<PromotionalCodeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionalCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
