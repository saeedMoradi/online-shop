import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-payment-cards',
  templateUrl: './payment-cards.component.html',
  styleUrls: ['./payment-cards.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentCardsComponent implements OnInit {
  createMode: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  createCard() {
    this.createMode = !this.createMode;
  }
}
