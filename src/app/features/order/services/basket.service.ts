import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getBasket(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/shopping_baskets`);
  }
  createBasket(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/shopping_baskets`, data);
  }
  deleteBasket(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/shopping_baskets/${id}`);
  }
  updateBasket(id: any, data: any): Observable<any> {
    return this.http.put<any>(
      `${this.apiUrl}/api/shopping_baskets/${id}`,
      data
    );
  }
  getShoppingOrders(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/shopping_orders`);
  }
  createShoppingOrders(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/shopping_orders`, data);
  }
  updateShoppingOrders(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/shopping_orders/${id}`, data);
  }
}
