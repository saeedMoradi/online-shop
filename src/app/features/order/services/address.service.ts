import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(private http: HttpClient) {}

  clearForm() {
    this.clearFormSource.next(true);
  }
  getAddresses(page = ''): Observable<any> {
    return this.http.get<any>(
      `${this.apiUrl}/api/addresses?order[id]=desc&${page}`
    );
  }
  createAddress(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/addresses`, data);
  }
  deleteAddress(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/addresses/${id}`);
  }
  putAddress(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/addresses/${id}`, data);
  }
}
