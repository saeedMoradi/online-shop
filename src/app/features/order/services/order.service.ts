import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  createOrder(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/orders`, data);
  }
  deleteOrder(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/orders/${id}`);
  }
  putOrder(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/orders/${id}`, data);
  }
  getPreOrder(shopId: any): Observable<any> {
    return this.http.get<any>(
      `${this.apiUrl}/api/pre_orders?shop.uuid=${shopId}`
    );
  }
}
