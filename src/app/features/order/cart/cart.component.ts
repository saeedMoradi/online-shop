import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  NgZone,
  AfterViewInit,
  OnDestroy,
  Inject,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  selectBasket,
  selectBasketTotal,
  selectBasketAndTotal
} from '../+store/order.selectors';
import {
  changeBasketQuantity,
  createShoppingBasket
} from '../+store/order.actions';
import {
  selectActiveShop,
  selectShopCurrency
} from '../../home/+store/shop.selector';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input('mode') mode = 'Delivery';
  currencySymbol$: Observable<any> = this.store.select(selectShopCurrency);
  activeShop;
  basket;
  basketTotal$: Observable<any> = this.store.select(selectBasketTotal);
  shopData$: Observable<any> = this.store.select(selectActiveShop);
  destroy$ = new Subject();

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private zone: NgZone,
    private store: Store,
    private cd: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.store
      .select(selectActiveShop)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.activeShop = { ...data };
      });
    this.store
      .select(selectBasket)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.basket = data;
        this.cd.detectChanges();
      });
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      fromEvent(window, 'scroll')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event: Event) => {
          let header: HTMLElement = this.document.querySelector('.header');
          let cart: HTMLElement = this.document.querySelector('.cart');
          let stickyCart = 0;
          if (stickyCart === 0) {
            stickyCart = header.offsetTop + 150;
          }
          if (window.pageYOffset > stickyCart) {
            cart.classList.add('sticky');
          } else {
            cart.classList.remove('sticky');
          }
        });
    });
  }

  add(item) {
    this.store.dispatch(changeBasketQuantity({ item, quantity: +1 }));
  }

  remove(item) {
    this.store.dispatch(changeBasketQuantity({ item, quantity: -1 }));
  }

  onChangeMode($event: string) {
    this.mode = $event;
  }

  checkout() {
    if (!this.basket.length) {
      return;
    }
    const basket = {
      platform: '',
      products: [],
      shop: this.activeShop['@id']
    };
    this.basket.forEach(element => {
      let accessories = [];
      let ingredients = [];
      const variants = element.selectedCombinations.map((el: any) => {
        return el.child['@id'];
      });
      element.selectedAccessories.forEach(el => {
        el.childs.forEach(e => {
          accessories.push(e['@id']);
        });
      });
      element.selectedIngredients.forEach(el => {
        el.ingredients.forEach(e => {
          ingredients.push({
            title: e.title,
            checked: e.checked
          });
        });
      });
      basket.products.push({
        count: element.quantity,
        detail: {
          product: element.product['@id'],
          variants,
          accessories,
          ingredients
        }
      });
    });
    this.store.dispatch(createShoppingBasket({ request: basket }));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
