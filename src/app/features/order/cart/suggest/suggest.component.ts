import { Component, OnInit, ChangeDetectionStrategy, NgZone, Inject } from '@angular/core';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-suggest',
  templateUrl: './suggest.component.html',
  styleUrls: ['./suggest.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SuggestComponent implements OnInit {
  destroy$ = new Subject();

  constructor(@Inject(DOCUMENT) private document: Document, private zone: NgZone) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    let list: HTMLElement = this.document.querySelector('.products__list--item');
    let isDown = false;
    let startX;
    let scrollLeft;
    this.zone.runOutsideAngular(() => {
      fromEvent(list, 'mousedown')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          isDown = true;
          // list.classList.add('active-drag');
          startX = e.pageX - list.offsetLeft;
          scrollLeft = list.scrollLeft;
        });
      fromEvent(list, 'mouseleave')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          isDown = false;
          // list.classList.remove('active-drag');
        });
      fromEvent(list, 'mouseup')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          isDown = false;
          // list.classList.remove('active-drag');
        });
      fromEvent(list, 'mousemove')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          if (!isDown) return;
          e.preventDefault();
          const x = e.pageX - list.offsetLeft;
          const walk = (x - startX) * 1;
          list.scrollLeft = scrollLeft - walk;
        });
    });
  }

  scrollLeftCategories(list: HTMLElement) {
    const itemWidth = 100;
    const margin = 20;
    const itemsLenght = 6;
    if (list.clientWidth < itemsLenght * (itemWidth + margin)) {
      list.scrollTo({ left: list.scrollLeft - 100, top: 0, behavior: 'smooth' });
    }
  }

  scrollRightCategories(list: HTMLElement) {
    const itemWidth = 100;
    const margin = 20;
    const itemsLenght = 6;
    if (list.clientWidth < itemsLenght * (itemWidth + margin)) {
      list.scrollTo({ left: list.scrollLeft + 100, top: 0, behavior: 'smooth' });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
