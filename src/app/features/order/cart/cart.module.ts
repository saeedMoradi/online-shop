import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { CartComponent } from './cart.component';
import { SuggestComponent } from './suggest/suggest.component';

@NgModule({
  declarations: [CartComponent, SuggestComponent],
  exports: [CartComponent, SuggestComponent],
  imports: [SharedModule]
})
export class CartModule {}
