import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
// import { AnimationItem } from 'lottie-web';
// import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-complete-order',
  templateUrl: './complete-order.component.html',
  styleUrls: ['./complete-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompleteOrderComponent implements OnInit {
  options: any = {
    path: '/assets/Chef.json',
    beforeAnimationIsCreated: (player: any): any => {
      const isSafari = /^((?!chrome|android).)*safari/i.test(
        navigator.userAgent
      );
      if (isSafari) {
        player.setLocationHref(location.href);
      }
    }
  };

  constructor(private router: Router) {}

  ngOnInit(): void {}

  // animationCreated(animationItem: AnimationItem): void {
  //   console.log(animationItem);
  // }

  trackOrder() {
    this.router.navigateByUrl('/order-now/track-order');
  }
}
