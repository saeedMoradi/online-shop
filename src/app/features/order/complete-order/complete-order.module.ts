import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompleteOrderRoutingModule } from './complete-order-routing.module';
import { CompleteOrderComponent } from './complete-order.component';
import { CartModule } from 'src/app/features/order/cart/cart.module';
import { LottieModule } from 'ngx-lottie';

export function playerFactory() {
  return import(
    /* webpackChunkName: 'lottie-web' */ 'lottie-web/build/player/lottie_svg'
  );
}

@NgModule({
  declarations: [CompleteOrderComponent],
  imports: [
    SharedModule,
    CartModule,
    CompleteOrderRoutingModule,
    LottieModule.forRoot({ player: playerFactory, useCache: true })
  ]
})
export class CompleteOrderModule {}
