import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompleteOrderComponent } from './complete-order.component';

const routes: Routes = [{ path: '', component: CompleteOrderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompleteOrderRoutingModule {}
