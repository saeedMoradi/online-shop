import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoogleMapsModule } from '@angular/google-maps'

import { CreateOrderComponent } from './create-order.component';

const routes: Routes = [{ path: '', component: CreateOrderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes), GoogleMapsModule],
  exports: [RouterModule]
})
export class CreateOrderRoutingModule {}
