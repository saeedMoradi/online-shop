import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isAccessoryChecked'
})
export class IsAccessoryCheckedPipe implements PipeTransform {
  transform(childId: number, parentId: number, list: []) {
    const findParent: any = list.find((element: any) => {
      return parentId === element.id;
    });
    if (findParent) {
      const findChild = findParent.childs.find((el: any) => {
        return childId === el.id;
      });
      if (findChild) {
        return true;
      }
    }
    return false;
  }
}
