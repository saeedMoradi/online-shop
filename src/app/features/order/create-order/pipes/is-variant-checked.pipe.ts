import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isVariantChecked'
})
export class IsVariantCheckedPipe implements PipeTransform {
  transform(id: number, list: []) {
    const find = list.find((element: any) => {
      return id === element.child.id;
    });
    if (find) {
      return true;
    }
    return false;
  }
}
