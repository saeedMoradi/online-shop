import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DialogRef } from '@ngneat/dialog';

@Component({
  selector: 'app-basket-mobile',
  templateUrl: './basket-mobile.component.html',
  styleUrls: ['./basket-mobile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasketMobileComponent implements OnInit {

  constructor(public ref: DialogRef,) { }

  ngOnInit(): void {
  }

  cancel() {
    this.ref.close()
  }

}
