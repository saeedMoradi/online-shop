import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import { ProductDialogMobileComponent } from '../product-dialog-mobile/product-dialog-mobile.component';
import { ProductDialogWebComponent } from '../product-dialog-web/product-dialog-web.component';
import { DialogService } from '@ngneat/dialog';
import { Store } from '@ngrx/store';
import { addToBasket, sumQuantity } from '../../+store/order.actions';
import { Observable, Subject } from 'rxjs';
import { productQuantityOnBasket, selectBasket, selectQuantity } from '../../+store/order.selectors';
import { takeUntil } from 'rxjs/operators';
import { selectShopCurrency } from 'src/app/features/home/+store/shop.selector';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {
  @Input('product') product;
  @Input('isHandset') isHandset: boolean;
  currencySymbol$: Observable<any> = this.store.select(selectShopCurrency);
  qtyOnBasket: number;
  destroy$ = new Subject();
  activeBar: boolean = false;
  productArray = [];

  constructor(
    private dialog: DialogService,
    private store: Store,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.store
      .select(productQuantityOnBasket, {
        id: this.product.id
      })
      .pipe(takeUntil(this.destroy$))
      .subscribe((qty: number) => {
        this.qtyOnBasket = qty;
        this.cd.detectChanges();
    });
  }

  public sumQuntity(nums) {
    return nums.reduce((a, b) => a + b);
  }

  addProduct() {
    this.store.select(selectBasket).subscribe(data => {
      if (data.length) {
        let quantity = data.map(quan => quan.quantity);
        let total = this.sumQuntity(quantity)
        this.store.dispatch(sumQuantity({
          quantity: total
        }))
      }
    })

    if (
      !this.product.productVariant.length &&
      !this.product.accessories.length &&
      !this.product.ingredients.length &&
      !this.product.description
    ) {
      this.store.dispatch(
        addToBasket({
          product: {
            product: this.product,
            quantity: 1,
            total: this.product.price,
            selectedAccessories: [],
            selectedCombinations: [],
            selectedIngredients: []
          }
        })
      );
    } else {
      // size: 'fullScreen'
      // windowClass: 'someClass'
      if (this.isHandset) {
        // MOBILE Version
        const dialogRef = this.dialog.open(ProductDialogMobileComponent, {
          data: this.product,
          closeButton: false,
          width: '100vw',
          height: '100vh',
          windowClass: 'mobile-dialog'
        });
        dialogRef.afterClosed$.subscribe(result => {
          if (!!result) {
            this.store.dispatch(addToBasket({ product: result }));
          }
        });
      } else {
        // WEB Version
        const dialogRef = this.dialog.open(ProductDialogWebComponent, {
          data: this.product,
          closeButton: false,
          width: '50rem',
          height: '90vh'
        });
        dialogRef.afterClosed$.subscribe(result => {
          if (!!result) {
            this.store.dispatch(addToBasket({ product: result }));
          }
        });
      }
    }
  }

  _addProduct() {
    this.activeBar = !this.activeBar;
    if (this.activeBar) {
      this.qtyOnBasket = 1;
    } else {
      this.qtyOnBasket = 0;
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
