import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  NgZone,
  Inject,
  Input
} from '@angular/core';
import { Observable, Subject, fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { OrderState } from '../../+store/order.interface';
import { select, Store } from '@ngrx/store';
import { changeActiveCategoryId } from '../../+store/order.actions';
import { selectActiveCategoryId } from '../../+store/order.selectors';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesComponent implements OnInit {
  destroy$ = new Subject();
  @Input() activeMenu;
  activeCategoryId;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private zone: NgZone,
    private store: Store<OrderState>
  ) {}

  ngOnInit(): void {
    this.store
      .pipe(select(selectActiveCategoryId), takeUntil(this.destroy$))
      .subscribe((id: any) => {
        if (id) {
          this.activeCategoryId = id;
          // if (id !== 'MealDeal') {
          //   this.store.dispatch(getProducts({ page: 'page=1', catId: id }));
          // }
        }
      });
  }

  selectMeal() {
    this.selectCat('MealDeal');
  }

  selectCat(id) {
    this.store.dispatch(changeActiveCategoryId({ id }));
  }

  ngAfterViewInit() {
    let list: HTMLElement = this.document.querySelector('.list');
    let categories: HTMLElement = this.document.querySelector('.categories');
    let catOffset = 450;
    let isDown = false;
    let startX;
    let scrollLeft;
    this.zone.runOutsideAngular(() => {
      // Sticky elements
      fromEvent(window, 'scroll')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event: Event) => {
          if (window.pageYOffset > catOffset) {
            categories.classList.add('sticky-cat');
          } else {
            categories.classList.remove('sticky-cat');
          }
        });
      // Drag Categories
      fromEvent(list, 'mousedown')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          isDown = true;
          // list.classList.add('active-drag');
          startX = e.pageX - list.offsetLeft;
          scrollLeft = list.scrollLeft;
        });
      fromEvent(list, 'mouseleave')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          isDown = false;
          // list.classList.remove('active-drag');
        });
      fromEvent(list, 'mouseup')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          isDown = false;
          // list.classList.remove('active-drag');
        });
      fromEvent(list, 'mousemove')
        .pipe(takeUntil(this.destroy$))
        .subscribe((e: MouseEvent) => {
          if (!isDown) return;
          e.preventDefault();
          const x = e.pageX - list.offsetLeft;
          const walk = (x - startX) * 1;
          list.scrollLeft = scrollLeft - walk;
        });
    });
  }

  scrollLeftCategories(list: HTMLElement) {
    list.scrollTo({ left: list.scrollLeft - 100, top: 0, behavior: 'smooth' });
  }

  scrollRightCategories(list: HTMLElement) {
    list.scrollTo({ left: list.scrollLeft + 100, top: 0, behavior: 'smooth' });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
