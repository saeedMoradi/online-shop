import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProductDialogMobileComponent } from './product-dialog-mobile.component';

describe('ProductDialogMobileComponent', () => {
  let component: ProductDialogMobileComponent;
  let fixture: ComponentFixture<ProductDialogMobileComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ProductDialogMobileComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDialogMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
