import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DialogRef } from '@ngneat/dialog';
import { Store } from '@ngrx/store';
import { selectShopCurrency } from 'src/app/features/home/+store/shop.selector';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-dialog-mobile',
  templateUrl: './product-dialog-mobile.component.html',
  styleUrls: ['./product-dialog-mobile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDialogMobileComponent implements OnInit {
  currencySymbol$: Observable<any> = this.store.select(selectShopCurrency);
  total: number = 0;
  quantitiy: number = 1;
  selectedCombinations = [];
  selectedAccessories = [];
  selectedIngredients = [];
  preSelectedIngredients = [];
  accessoryNumner: number = 0;
  // activeChoose: boolean = false;
  // activeChooseTwo: boolean = false;
  // activeChooseThree: boolean = false;
  // activeHeaderChoose: boolean = false;
  // activePick: boolean = false;
  // activePickTwo: boolean = false;
  // activeHeaderPick: boolean = false;

  constructor(public ref: DialogRef, private store: Store, ) {}

  ngOnInit(): void {
    // TODO: crete form for data
    this.selectedIngredients = this.ref.data.ingredients
      .filter((item: any) => {
        if (item.ingredients.length) {
          return true;
        }
        return false;
      })
      .map(element => {
        return {
          ...element,
          ingredients: element.ingredients.filter((el: any) => {
            if (el.checked) {
              return true;
            }
            return false;
          })
        };
      });
    this.preSelectedIngredients = [...this.selectedIngredients];
    if (this.ref.data.productVariant?.length) {
      this.ref.data.productVariant.forEach(element => {
        element.productVariants.forEach(el => {
          if (el.variants[0].active) {
            this.selectedCombinations.push({
              '@id': element['@id'],
              id: element.id,
              title: element.title,
              child: { ...el }
            });
          }
        });
      });
    }
    if (this.ref.data.accessories?.length) {
      this.ref.data.accessories.forEach(element => {
        let childs = [];
        element.productHasAccessories.forEach(el => {
          if (el.accessory.active) {
            childs.push(el.accessory);
          }
        });
        if (childs.length) {
          this.selectedAccessories.push({
            ...element.accessory,
            childs
          });
        }
      });
    }
    this.calcualteTotal();
  }

  removeOne() {
    if (this.quantitiy > 1) {
      this.quantitiy = this.quantitiy - 1;
      this.calcualteTotal();
    }
  }

  addOne() {
    this.quantitiy = this.quantitiy + 1;
    this.calcualteTotal();
  }

  selectAccessory(e, accessory) {
    if (e.target.checked) {
      this.selectedAccessories = [...this.selectedAccessories, accessory];
    } else {
      this.selectedAccessories = this.selectedAccessories.filter(
        (item: any) => {
          return item.accessory['@id'] !== accessory.accessory['@id'];
        }
      );
    }
    this.calcualteTotal();
  }

  calcualteTotal() {
    const accessorySum = this.selectedAccessories.reduce((prev, curr) => {
      return prev + curr.price;
    }, 0);

    const combinationsSum = this.selectedCombinations.reduce((prev, curr) => {
      return prev + curr.child.price;
    }, 0);

    this.total =
      this.quantitiy * (this.ref.data.price + combinationsSum + accessorySum);
  }

  changeCombination(e, variantHead, variantChild) {
    const find = this.selectedCombinations.find((varHead: any) => {
      return variantHead.id === varHead.id;
    });
    if (find) {
      find.child = { ...variantChild };
    } else {
      this.selectedCombinations = [
        ...this.selectedCombinations,
        {
          '@id': variantHead['@id'],
          id: variantHead.id,
          title: variantHead.title,
          child: { ...variantChild }
        }
      ];
    }
    this.calcualteTotal();
  }

  selectIngredient(e, parent, child) {
    const findParentIndex = this.selectedIngredients.findIndex((data: any) => {
      return data.id === parent.id;
    });
    if (e.target.checked) {
      if (findParentIndex >= 0) {
        this.selectedIngredients[findParentIndex].ingredients.push({
          ...child,
          checked: true
        });
      } else {
        this.selectedIngredients.push({
          '@id': parent['@id'],
          id: parent.id,
          ingredients: [{ ...child, checked: true }]
        });
      }
    } else {
      const parentItem = this.selectedIngredients[findParentIndex];
      if (parentItem.ingredients.length === 1) {
        this.selectedIngredients.splice(findParentIndex, 1);
      } else {
        const findChildIndex = parentItem.ingredients.findIndex((el: any) => {
          return el.id === child.id;
        });
        this.selectedIngredients = this.selectedIngredients.map((item: any) => {
          if (item.id === parent.id) {
            return {
              ...item,
              ingredients: parentItem.ingredients.filter((data: any) => {
                if (data.id === parentItem.ingredients[findChildIndex].id) {
                  return false;
                }
                return true;
              })
            };
          }
          return item;
        });
      }
    }
  }

  cancel() {
    this.ref.close();
  }

  public addAccessories(parent, child) {

    const findParentIndex = this.selectedAccessories.findIndex((data: any) => {
      return data.id === parent.accessory.id
    });

    if (findParentIndex >= 0) {

      if (this.accessoryNumner < child.max) {
        this.selectedAccessories[findParentIndex].qtty+=1;
        this.accessoryNumner +=1;
        this.total += child.price;
      }
    }else {
      if (this.accessoryNumner < child.max) {
        this.selectedAccessories.push({
          ...child,
          qtty: 1
        });
        this.accessoryNumner +=1;
        this.total += child.price;
      }
    }
  }

  public minusAdditional(accessory) {
    const findParentIndex = this.selectedAccessories.findIndex((data: any) => {
      return data.id === accessory.id;
    });

    if (findParentIndex >= 0) {
      this.selectedAccessories[findParentIndex].qtty-=1;
      this.accessoryNumner -= 1;
      this.total -= this.selectedAccessories[findParentIndex].price
    }

  }

  done() {
    if (
      this.selectedCombinations.length !== this.ref.data.productVariant.length
    ) {
      return;
    }
    const currentObj = {};
    this.selectedIngredients.forEach((currParentIng: any) => {
      currParentIng.ingredients.forEach(currChildIng => {
        if (currentObj[currParentIng.id]) {
          currentObj[currParentIng.id] = [
            ...currentObj[currParentIng.id],
            currChildIng.id
          ];
        } else {
          currentObj[currParentIng.id] = [currChildIng.id];
        }
      });
    });
    this.preSelectedIngredients.forEach((preParentIng: any) => {
      if (currentObj[preParentIng.id]) {
        preParentIng.ingredients.forEach(preChildIng => {
          const find = currentObj[preParentIng.id].find((item: any) => {
            return item === preChildIng.id;
          });
          if (!find) {
            this.selectedIngredients = this.selectedIngredients.map(
              (item: any) => {
                if (item.id === preParentIng.id) {
                  return {
                    ...item,
                    ingredients: [
                      ...item.ingredients,
                      { ...preChildIng, checked: false }
                    ]
                  };
                }
                return item;
              }
            );
          }
        });
      } else {
        this.selectedIngredients = [
          ...this.selectedIngredients,
          {
            ...preParentIng,
            ingredients: preParentIng.ingredients.map((item: any) => {
              return { ...item, checked: false };
            })
          }
        ];
      }
    });
    this.ref.close({
      product: this.ref.data,
      quantity: this.quantitiy,
      total: this.total,
      selectedAccessories: this.selectedAccessories,
      selectedCombinations: this.selectedCombinations,
      selectedIngredients: this.selectedIngredients
    });
  }

}
