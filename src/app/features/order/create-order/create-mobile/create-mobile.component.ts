import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ElementRef,
  ViewChild,
  Renderer2
} from '@angular/core';
import { Location } from '@angular/common';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-create-mobile',
  templateUrl: './create-mobile.component.html',
  styleUrls: ['./create-mobile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateMobileComponent implements OnInit {
  @ViewChild('menu', { static: true }) menu: ElementRef<HTMLButtonElement>;
  @ViewChild('info', { static: true }) info: ElementRef<HTMLButtonElement>;
  @ViewChild('bar', { static: true }) bar: ElementRef<HTMLDivElement>;
  @Input('activeShop') activeShop;
  @Input('activeMenu') activeMenu;
  @Input('isHandset') isHandset;
  tab: string = 'Menu';
  reserve_del_col: boolean = false;
  active: string = 'delivery';
  _active: string = 'when';
  destroy$ = new Subject();

  constructor(private renderer: Renderer2, private location: Location) {}

  ngOnInit(): void {
    fromEvent(window, 'resize')
      .pipe(takeUntil(this.destroy$), debounceTime(300))
      .subscribe(event => {
        this.setBarForTab();
      });
  }

  ngAfterViewInit() {
    this.setBarForTab();
  }

  setBarForTab() {
    if (this.tab === 'Menu') {
      const offsetLeft = this.menu.nativeElement.offsetLeft;
      this.renderer.setStyle(
        this.bar.nativeElement,
        'transform',
        `translateX(${offsetLeft}px)`
      );
    } else {
      const offsetLeft = this.info.nativeElement.offsetLeft;
      this.renderer.setStyle(
        this.bar.nativeElement,
        'transform',
        `translateX(${offsetLeft}px)`
      );
    }
  }

  changeTab(data: string) {
    this.tab = data;
    this.setBarForTab();
  }

  previousPage() {
    this.location.back();
  }

  public del_col() {
    this.reserve_del_col = !this.reserve_del_col;
    // console.log(this.reserve_del_col = !this.reserve_del_col)
  }

  public changeService(data: string) {
    this.active = data;
  }

  public changeWhen(data: string) {
    this._active = data;
  }
}
