import { Component, OnInit, ChangeDetectionStrategy, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { MapInfoWindow, MapMarker, GoogleMap } from '@angular/google-maps'

@Component({
  selector: 'app-map-info',
  templateUrl: './map-info.component.html',
  styleUrls: ['./map-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapInfoComponent implements OnInit {

  @ViewChild(MapInfoWindow) infoWindow: MapInfoWindow;
  @ViewChild('marker') marker: MapMarker;
  @ViewChild('googleMap') googleMap: GoogleMap;
  // activeShop;
  @Input() center;
  zoom = 15;
  markerOptions = { 
    draggable: false,
    icon: 'assets/images/location-pin.svg'
   };
  @Input() markerPositions: google.maps.LatLngLiteral[];
  options = {
    mapTypeControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    scrollwheel: false
  };
  constructor() { }

  ngOnInit(): void {

  }

}
