import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';

import { SharedModule } from 'src/app/shared/shared.module';
import { CreateOrderRoutingModule } from './create-order-routing.module';
import { CategoriesComponent } from './categories/categories.component';
import { ProductGroupComponent } from './product-group/product-group.component';
import { ProductComponent } from './product/product.component';
import { ProductDialogMobileComponent } from './product-dialog-mobile/product-dialog-mobile.component';
import { ProductDialogWebComponent } from './product-dialog-web/product-dialog-web.component';
import { CreateOrderComponent } from './create-order.component';
import { CartModule } from 'src/app/features/order/cart/cart.module';
import { InformationComponent } from './information/information.component';
import { MapInfoComponent } from './map-info/map-info.component';
import { ServiceRestaurantComponent } from './service-restaurant/service-restaurant.component';
import { CreateWebComponent } from './create-web/create-web.component';
import { CreateMobileComponent } from './create-mobile/create-mobile.component';
import { BasketMobileComponent } from './basket-mobile/basket-mobile.component';
// PIPES
import { IsVariantCheckedPipe } from './pipes/is-variant-checked.pipe';
import { IsAccessoryCheckedPipe } from './pipes/is-accessory-checked.pipe';

@NgModule({
  declarations: [
    CreateOrderComponent,
    CategoriesComponent,
    ProductComponent,
    ProductDialogMobileComponent,
    ProductDialogWebComponent,
    ProductGroupComponent,
    IsVariantCheckedPipe,
    IsAccessoryCheckedPipe,
    InformationComponent,
    MapInfoComponent,
    ServiceRestaurantComponent,
    CreateWebComponent,
    CreateMobileComponent,
    BasketMobileComponent
  ],
  imports: [
    SharedModule,
    CartModule,
    CreateOrderRoutingModule,
    GoogleMapsModule
  ]
})
export class CreateOrderModule {}
