import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from '@angular/core';
import { Store } from '@ngrx/store';
import { selectCategory } from '../../+store/order.selectors';

@Component({
  selector: 'app-product-group',
  templateUrl: './product-group.component.html',
  styleUrls: ['./product-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductGroupComponent implements OnInit {
  @Input('isHandset') isHandset: boolean;
  category$ = this.store.select(selectCategory);

  constructor(private store: Store<any>) {}

  ngOnInit(): void {}
}
