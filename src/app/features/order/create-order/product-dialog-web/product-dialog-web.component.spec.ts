import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProductDialogWebComponent } from './product-dialog-web.component';

describe('ProductDialogWebComponent', () => {
  let component: ProductDialogWebComponent;
  let fixture: ComponentFixture<ProductDialogWebComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ProductDialogWebComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDialogWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
