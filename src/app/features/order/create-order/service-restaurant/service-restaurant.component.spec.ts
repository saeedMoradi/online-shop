import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceRestaurantComponent } from './service-restaurant.component';

describe('ServiceRestaurantComponent', () => {
  let component: ServiceRestaurantComponent;
  let fixture: ComponentFixture<ServiceRestaurantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
