import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, ChangeDetectorRef, Input } from '@angular/core';
import { BasketMobileComponent } from '../basket-mobile/basket-mobile.component';
import { DialogService } from '@ngneat/dialog'
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectQuantity } from '../../+store/order.selectors';

@Component({
  selector: 'app-service-restaurant',
  templateUrl: './service-restaurant.component.html',
  styleUrls: ['./service-restaurant.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceRestaurantComponent implements OnInit {

  @Output() showService = new EventEmitter<boolean>();
  iconRotate: boolean = false;
  reserveService;
  sumQuantity: Observable<any> = this.store.select(selectQuantity);
  number: number = 0;
  @Input('isHandset') isHandset: boolean;

  constructor(private store: Store, private cd: ChangeDetectorRef, private dialog: DialogService) { }

  ngOnInit(): void {
    this.sumQuantity.subscribe(data => {
      if (data >= 1) {
        this.number = data
        this.cd.detectChanges()
      }
    })
  }

  public del_col() {
    this.showService.emit(this.reserveService); 
    this.iconRotate = !this.iconRotate
  }

  showBasketMobile() {
    // if (this.isHandset) {
      const dialog = this.dialog.open(BasketMobileComponent, {
        closeButton: false,
        width: '100vw',
        height: '100vh',
        windowClass: 'mobile-dialog'
      })
    // }
  }

}
