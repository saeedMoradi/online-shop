import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  NgZone,
  Inject,
  ChangeDetectorRef,
  HostListener
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  selectActiveMenu,
  selectActiveShop
} from '../../home/+store/shop.selector';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateOrderComponent implements OnInit {
  activeShop$: Observable<any> = this.store.select(selectActiveShop);
  activeMenu$: Observable<any> = this.store.select(selectActiveMenu);
  isHandset: boolean;
  destroy$ = new Subject();

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private zone: NgZone,
    private store: Store<any>,
    private cd: ChangeDetectorRef,
    breakpointObserver: BreakpointObserver
  ) {
    breakpointObserver
      .observe(['(max-width: 768px)'])
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        this.isHandset = result.matches;
        this.cd.markForCheck();
      });
  }

  ngOnInit(): void {
    // this.activeShop$.subscribe(res => {
    //   if (res) {
    //     console.log(res)
    //   }
    // })
  }

  ngAfterViewInit() {
    let categories: HTMLElement = this.document.querySelector('.categories');
    let body: HTMLElement = this.document.querySelector('.body');
    let stickyCategories = categories.offsetTop + 150;
    this.zone.runOutsideAngular(() => {
      fromEvent(window, 'scroll')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event: Event) => {
          if (window.pageYOffset > stickyCategories) {
            body.classList.add('margin-for-stciky');
          } else {
            body.classList.remove('margin-for-stciky');
          }
        });
    });
  }

  goToBasket() {}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
