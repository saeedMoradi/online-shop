import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InformationComponent implements OnInit {

  infoPostalCode: string; 
  infoPhone: string; 
  _center;
  _markerPositions;
  showDay
  @Input() activeShop;

  workTimes = [
    {
      '@id': "/api/worktimes/12",
      '@type': "Worktime",
      'close': "1970-01-01T10:02:00+00:00",
      'id': 12,
      'is24': true,
      'open': "1970-01-01T04:00:00+00:00",
      days: [
        {
          '@id': "_:3064",
          '@type': "Day",
          'name': "friday",
        },
        {
          '@id': "_:3070",
          '@type': "Day",
          'name': "thursday",
        }
      ]
    },
    {
      '@id': "/api/worktimes/13",
      '@type': "Worktime",
      'close': "1970-01-01T10:04:00+00:00",
      'id': 13,
      'is24': false,
      'open': "1970-01-01T10:04:00+00:00",
      days: [
        {
          '@id': "_:3076",
          '@type': "Day",
          'name': "thursday",
        },
        {
          '@id': "_:3079",
          '@type': "Day",
          'name': "friday",
        },
        {
          '@id': "_:3080",
          '@type': "Day",
          'name': "saturday",
        }
      ]
    }
  ]
  workTime;
  timeWork_open: any;
  timeWork_close: any;
  today: string;
  
  constructor() { }

  ngOnInit(): void {    

    switch (new Date().getDay()) {
      case 0:
        this.today = "Sunday";
        break;
      case 1:
        this.today = "Monday";
        break;
      case 2:
         this.today = "Tuesday";
        break;
      case 3:
        this.today = "Wednesday";
        break;
      case 4:
        this.today = "Thursday";
        break;
      case 5:
        this.today = "Friday";
        break;
      case 6:
        this.today = "Saturday";
    }

    this.infoPhone = this.activeShop?.phone;
    this.infoPostalCode = `${this.activeShop?.country} ${this.activeShop?.address}`;
    this._markerPositions  = [{ 
      lat: +this.activeShop?.latitude, 
      lng: +this.activeShop?.longitude
    }];
    this._center = { 
      lat: +this.activeShop?.latitude, 
      lng: +this.activeShop?.longitude
    }
  }

}
