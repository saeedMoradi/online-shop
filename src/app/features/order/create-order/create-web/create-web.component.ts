import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from '@angular/core';

@Component({
  selector: 'app-create-web',
  templateUrl: './create-web.component.html',
  styleUrls: ['./create-web.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateWebComponent implements OnInit {
  @Input('activeShop') activeShop;
  @Input('activeMenu') activeMenu;

  constructor() {}

  ngOnInit(): void {}
}
