import { NgModule } from '@angular/core';

import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { OrderEffects } from './+store/order.effects';

@NgModule({
  declarations: [OrderComponent],
  imports: [
    SharedModule,
    OrderRoutingModule,
    EffectsModule.forFeature([OrderEffects])
  ]
})
export class OrderModule {}
