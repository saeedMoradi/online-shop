import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CartModule } from 'src/app/features/order/cart/cart.module';
import { TrackOrderRoutingModule } from './track-order-routing.module';
import { TrackOrderComponent } from './track-order.component';

@NgModule({
  declarations: [TrackOrderComponent],
  imports: [SharedModule, CartModule, TrackOrderRoutingModule]
})
export class TrackOrderModule {}
