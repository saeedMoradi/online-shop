import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { selectOrderStatus } from '../+store/order.selectors';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrackOrderComponent implements OnInit {
  currenctOrderStatus;
  destroy$ = new Subject();

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store
      .select(selectOrderStatus)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.currenctOrderStatus = data;
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
