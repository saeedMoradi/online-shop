import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderComponent } from './order.component';

const routes: Routes = [
  {
    path: '',
    component: OrderComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./create-order/create-order.module').then(
            m => m.CreateOrderModule
          )
      },
      {
        path: 'place-order',
        loadChildren: () =>
          import('./place-order/place-order.module').then(
            m => m.PlaceOrderModule
          )
      },
      {
        path: 'complete-order',
        loadChildren: () =>
          import('./complete-order/complete-order.module').then(
            m => m.CompleteOrderModule
          )
      },
      {
        path: 'track-order',
        loadChildren: () =>
          import('./track-order/track-order.module').then(
            m => m.TrackOrderModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule {}
