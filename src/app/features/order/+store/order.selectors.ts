import { createSelector, createFeatureSelector } from '@ngrx/store';
import { OrderState } from './order.interface';
import { Basket } from 'src/app/shared/interfaces';
import { featureKey } from './order.reducer';
import { selectActiveMenu } from '../../home/+store/shop.selector';

export const selectFeature = createFeatureSelector<OrderState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: OrderState) => state.error
);

export const selectIsLoading = createSelector(
  selectFeature,
  (state: OrderState) => state.isLoading
);



export const selectActiveCategoryId = createSelector(
  selectFeature,
  (state: OrderState) => state.activeCategoryId
);
export const selectCategory = createSelector(
  selectActiveMenu,
  selectActiveCategoryId,
  (activeMenu, categoryId) => {
    if (!activeMenu || !categoryId) {
      return null;
    }
    const find = activeMenu.categories.find((data: any) => {
      return data.id === categoryId;
    });
    if (find) {
      return find;
    }
    return null;
  }
);
export const selectBasket = createSelector(
  selectFeature,
  (state: OrderState) => state.basket
);
export const selectQuantity = createSelector(
  selectFeature,
  (state: OrderState) => state.sumQuantity
);
export const preOrder = createSelector(
  selectFeature,
  (state: OrderState) => state?.preOrder['hydra:member'][0]
);
export const selectSubmitedBasketDetails = createSelector(
  selectFeature,
  (state: OrderState) => state.submitedBasketDetails
);
export const selectOrderDetails = createSelector(
  selectFeature,
  (state: OrderState) => state.orderDetails
);
export const selectOrderStatus = createSelector(
  selectOrderDetails,
  (order: any) => order?.status
);
export const selectBasketTotal = createSelector(selectBasket, basket => {
  if (basket.length) {
    const sum = basket.reduce((prev, curr) => {
      return prev + curr.total;
    }, 0);
    return sum;
  }
  return 0;
});
export const selectBasketTotalItems = createSelector(selectBasket, basket => {
  if (basket?.length) {
    const sum = basket.reduce((prev, curr) => {
      return prev + curr.quantity;
    }, 0);
    return sum;
  }
  return 0;
});

export const selectBasketAndTotal = createSelector(
  selectBasket,
  selectBasketTotal,
  (basket, total) => ({ basket, total })
);
export const selectMealdeals = createSelector(
  selectFeature,
  (state: OrderState) => state.mealdeals
);
export const productQuantityOnBasket = createSelector(
  selectBasket,
  (basket: Basket[], props) => {
    let qty = 0;
    basket.forEach((item: Basket) => {
      if (item.product.id === props.id) {
        qty = qty + item.quantity;
      }
    });
    return qty;
  }
);
