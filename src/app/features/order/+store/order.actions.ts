import { createAction, props } from '@ngrx/store';

export const getShoppingBasket = createAction('[Order] Get Shopping Basket');
export const getShoppingBasketSuccess = createAction(
  '[Order] Get Shopping Basket Success',
  props<{ response: any }>()
);
export const getShoppingBasketFail = createAction(
  '[Order] Get Shopping Basket Fail',
  props<{ error: any }>()
);
export const createShoppingBasket = createAction(
  '[Order] Create Shopping Basket',
  props<{ request: any }>()
);
export const createShoppingBasketSuccess = createAction(
  '[Order] Create Shopping Basket Success',
  props<{ response: any }>()
);
export const createShoppingBasketFail = createAction(
  '[Order] Create Shopping Basket Fail',
  props<{ error: any }>()
);
export const updateShoppingBasket = createAction(
  '[Order] Update Shopping Basket',
  props<{ request: any }>()
);
export const updateShoppingBasketSuccess = createAction(
  '[Order] Update Shopping Basket Success',
  props<{ response: any }>()
);
export const updateShoppingBasketFail = createAction(
  '[Order] Update Shopping Basket Fail',
  props<{ error: any }>()
);
export const getAddresses = createAction('[Categories] Get Addresses');
export const getAddressesSuccess = createAction(
  '[Order] Get Addresses Success',
  props<{ response: any }>()
);
export const getAddressesFail = createAction(
  '[Order] Get Addresses Fail',
  props<{ error: any }>()
);
export const createAddress = createAction(
  '[Categories] Delete Address',
  props<{ request: any }>()
);
export const createAddressSuccess = createAction(
  '[Order] Delete Address Success',
  props<{ response: any }>()
);
export const createAddressFail = createAction(
  '[Order] Delete Address Fail',
  props<{ error: any }>()
);
export const deleteAddress = createAction('[Categories] Delete Address');
export const deleteAddressSuccess = createAction(
  '[Order] Delete Address Success',
  props<{ response: any }>()
);
export const deleteAddressFail = createAction(
  '[Order] Delete Address Fail',
  props<{ error: any }>()
);
export const changeActiveCategoryId = createAction(
  '[Order] Change Active Category',
  props<{ id: any }>()
);
export const addToBasket = createAction(
  '[Order] Add To Basket',
  props<{ product: any }>()
);


export const sumQuantity = createAction(
  '[Order] Sum Quantity',
  props<{ quantity: any }>()
);


export const changeBasketQuantity = createAction(
  '[Order] Change Basket Quantity',
  props<{ item: any; quantity: any }>()
);
export const getBasketOrders = createAction('[Order] Get Basket Orders');
export const getBasketOrdersSuccess = createAction(
  '[Order] Get Basket Orders Success',
  props<{ response: any }>()
);
export const getBasketOrdersFail = createAction(
  '[Order] Get Basket Orders Fail',
  props<{ error: any }>()
);
export const createBasketOrder = createAction(
  '[Order] Create Basket Order',
  props<{ request: any }>()
);
export const createBasketOrderSuccess = createAction(
  '[Order] Create Basket Order Success',
  props<{ response: any }>()
);
export const createBasketOrderFail = createAction(
  '[Order] Create Basket Order Fail',
  props<{ error: any }>()
);
export const updateBasketOrder = createAction(
  '[Order] Update Basket Order',
  props<{ request: any }>()
);
export const updateBasketOrderSuccess = createAction(
  '[Order] Update Basket Order Success',
  props<{ response: any }>()
);
export const updateBasketOrderFail = createAction(
  '[Order] Update Basket Order Fail',
  props<{ error: any }>()
);
export const getOrderStatus = createAction(
  '[Order] Get Order Status',
  props<{ orderId?: number }>()
);
export const getOrderStatusSuccess = createAction(
  '[Order] Get Order Status Success',
  props<{ response: any }>()
);
export const getOrderStatusFail = createAction(
  '[Order] Get Order Status Fail',
  props<{ error: any }>()
);
export const getPreOrder = createAction(
  '[PreOrder] Get PreOrder',
  props<{ shopId: number }>()
);
export const getPreOrderSuccess = createAction(
  '[PreOrder] Get PreOrder Success',
  props<{ response: any }>()
);
export const getPreOrderFail = createAction(
  '[PreOrder] Get PreOrder Faild',
  props<{ error: any }>()
);
