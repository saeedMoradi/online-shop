import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './order.actions';
import { OrderState } from './order.interface';
import { v4 as uuidv4 } from 'uuid';

export const initialState: OrderState = {
  error: null,
  isLoading: false,
  activeCategoryId: null,
  mealdeals: null,
  basket: [],
  submitedBasketDetails: null,
  orderDetails: null,
  preOrder: null,
  sumQuantity: null
};

export const featureKey = 'order';

const orderReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): OrderState => {
      return {
        ...state,
        activeCategoryId: null
      };
    }
  ),
  on(
    fromActions.changeActiveCategoryId,
    (state, action): OrderState => ({
      ...state,
      activeCategoryId: action.id
    })
  ),
  on(
    fromActions.addToBasket,
    (state, { product }): OrderState => {
      let basket;
      const findIndex = state.basket.findIndex((item: any) => {
        return item.product.id === product.product.id;
      });
      if (findIndex >= 0) {
        if (
          !product.selectedAccessories.length &&
          !product.selectedCombinations.length &&
          !product.selectedIngredients.length &&
          !state.basket[findIndex].selectedAccessories.length &&
          !state.basket[findIndex].selectedCombinations.length &&
          !state.basket[findIndex].selectedIngredients.length
        ) {
          basket = state.basket.map((data: any, index) => {
            if (findIndex === index) {
              const total = calcTotal(
                data.product.price,
                data.selectedAccessories,
                data.selectedCombinations,
                data.quantity + 1
              );
              return {
                ...data,
                total,
                quantity: data.quantity + 1
              };
            }
            return data;
          });
        } else {
          basket = [...state.basket, { ...product, uuid: uuidv4() }];
        }
      } else {
        basket = [...state.basket, { ...product, uuid: uuidv4() }];
      }
      return {
        ...state,
        basket
      };
    }
  ),

  on(
    fromActions.sumQuantity,
    (state, action) => {
      return {
        ... state,
        sumQuantity: action.quantity
      }
    }
  ),

  on(
    fromActions.createShoppingBasketSuccess,
    (state, action): OrderState => {
      return {
        ...state,
        submitedBasketDetails: action.response
      };
    }
  ),
  on(
    fromActions.createBasketOrderSuccess,
    (state, action): OrderState => {
      return {
        ...state,
        orderDetails: action.response
      };
    }
  ),
  on(
    fromActions.changeBasketQuantity,
    (state, { item, quantity }): OrderState => {
      const basket = state.basket
        .map((data: any) => {
          if (data.uuid === item.uuid) {
            const total = calcTotal(
              data.product.price,
              data.selectedAccessories,
              data.selectedCombinations,
              data.quantity + quantity
            );
            return {
              ...data,
              total,
              quantity: data.quantity + quantity
            };
          }
          return data;
        })
        .filter((element: any) => {
          if (element.uuid === item.uuid) {
            if (element.quantity < 1) {
              return false;
            }
            return true;
          }
          return true;
        });
      return {
        ...state,
        basket
      };
    }
  ),
  on(
    fromActions.getPreOrderSuccess,
    (state, action): OrderState => {
      return {
        ...state,
        preOrder: action.response
      };
    }
  )
);

export function reducer(state: OrderState, action: Action) {
  return orderReducer(state, action);
}

function calcTotal(productPrice, accessories, combinations, qty) {
  let accSum = 0;
  accessories.forEach(element => {
    element.childs.forEach(el => {
      accSum = accSum + el.price;
    });
  });
  const comSum = combinations.reduce((prev, curr) => {
    return prev + curr.child.price;
  }, 0);
  return qty * (productPrice + accSum + comSum);
}
