import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { OrderService } from '../services/order.service';
import { BasketService } from '../services/basket.service';
import * as fromActions from './order.actions';

@Injectable()
export class OrderEffects {
  constructor(
    private actions$: Actions,
    private basketService: BasketService,
    private orderService: OrderService,
    private router: Router
  ) {}

  createShoppingBasket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createShoppingBasket),
      switchMap(({ request }) => {
        return this.basketService.createBasket(request).pipe(
          map((data: any) => {
            let url = this.router.url            
            this.router.navigateByUrl(`${url}/place-order`);
            return fromActions.createShoppingBasketSuccess({ response: data });
          }),
          catchError(error => {
            return of(fromActions.createShoppingBasketFail({ error }));
          })
        );
      })
    )
  );

  getPreOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPreOrder),
      switchMap(({ shopId }) => {
        return this.orderService.getPreOrder(shopId).pipe(
          map((data: any) => {
            return fromActions.getPreOrderSuccess({ response: data });
          }),
          catchError(error => {
            return of(fromActions.getPreOrderFail({ error }));
          })
        );
      })
    )
  );

  createBasketOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createBasketOrder),
      switchMap(({ request }) => {
        return this.basketService.createShoppingOrders(request).pipe(
          map((data: any) => {
            console.log(data);
            // this.router.navigateByUrl('/order-now/complete-order');
            return fromActions.createBasketOrderSuccess({ response: data });
          }),
          catchError(error => {
            return of(fromActions.createBasketOrderFail({ error }));
          })
        );
      })
    )
  );
}
