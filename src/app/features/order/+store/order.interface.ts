import { Basket } from 'src/app/shared/interfaces';

export interface OrderState {
  error: any;
  isLoading: boolean;
  activeCategoryId: string;
  mealdeals: any;
  basket: Basket[];
  submitedBasketDetails: any;
  orderDetails: any;
  preOrder: any;
  sumQuantity: any;
}
