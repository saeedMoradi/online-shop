import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { SPINNER } from 'ngx-ui-loader';
import { getServiceTypes } from './+store/shop.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  public SPINNER = SPINNER;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(getServiceTypes());
  }
}
