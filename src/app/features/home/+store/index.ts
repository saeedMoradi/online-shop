import * as shopReducer from './shop.reducer';
import { ShopState } from './shop.interface';
import * as shopSelector from './shop.selector';

export { shopReducer, shopSelector, ShopState }