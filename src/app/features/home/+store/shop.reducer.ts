import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './shop.actions';
import { ShopState } from './shop.interface';

export const initialState: ShopState = {
  error: null,
  isLoading: false,
  shops: null,
  postCode: null,
  activeShop: null,
  serviceTypes: null,
  menus: null,
  activeMenu: null,
  activeMerchant: null
};

export const featureKey = 'shop';

const shopReducer = createReducer(
  initialState,
  on(
    fromActions.getMerchant,
    (state, action): ShopState => ({
      ...state,
      isLoading: true
    })
  ),
  on(
    fromActions.getMerchantSuccess,
    (state, action): ShopState => ({
      ...state,
      isLoading: false,
      activeMerchant: action.response['hydra:member'][0] || 'not-found'
    })
  ),
  on(
    fromActions.getMerchantFail,
    (state, action): ShopState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.getShopData,
    (state, action): ShopState => ({
      ...state,
      isLoading: true
    })
  ),
  on(
    fromActions.getShopDataSuccess,
    (state, action): ShopState => ({
      ...state,
      error: null,
      isLoading: false,
      shops: action.response,
      activeShop: action.response['hydra:member'][0] || 'not-found'
    })
  ),
  on(
    fromActions.resetActiveShop,
    (state, action): ShopState => ({
      ...state,
      activeShop: null
    })
  ),
  on(
    fromActions.getShopDataFail,
    (state, action): ShopState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.getMenus,
    (state, action): ShopState => ({
      ...state,
      isLoading: true
    })
  ),
  on(
    fromActions.getMenusSuccess,
    (state, action): ShopState => {
      return {
        ...state,
        error: null,
        isLoading: false,
        menus: action.response,
        activeMenu: action.response['hydra:member'][0] || 'not-found'
      };
    }
  ),
  on(
    fromActions.getMenusFail,
    (state, action): ShopState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.changePostCode,
    (state, action): ShopState => ({
      ...state,
      postCode: action.postCode
    })
  ),
  on(
    fromActions.getServiceTypesSuccess,
    (state, action): ShopState => ({
      ...state,
      serviceTypes: action.response
    })
  )
);

export function reducer(state: ShopState, action: Action) {
  return shopReducer(state, action);
}
