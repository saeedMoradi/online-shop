import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ShopState } from './shop.interface';
import { featureKey } from './shop.reducer';

export const selectFeature = createFeatureSelector<ShopState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: ShopState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: ShopState) => state.isLoading
);
export const selectActiveMerchant = createSelector(
  selectFeature,
  (state: ShopState) => state?.activeMerchant
);
export const selectActiveShop = createSelector(
  selectFeature,
  (state: ShopState) => state?.activeShop
);
export const selectActiveMenu = createSelector(
  selectFeature,
  (state: ShopState) => {
    return state.activeMenu;
  }
);
export const selectServiceTypes = createSelector(
  selectFeature,
  (state: ShopState) => {
    if (state?.serviceTypes) {
      return state.serviceTypes['hydra:member'];
    }
    return null;
  }
);
export const selectMerchantShopMenu = createSelector(
  selectActiveMerchant,
  selectActiveShop,
  selectActiveMenu,
  (merchant, shop, menu) => {
    return { merchant, shop, menu };
  }
);
export const selectShopCurrency = createSelector(
  selectActiveShop,
  activeRestaurant => {
    return activeRestaurant?.currency?.title ?? 'GBP';
  }
);
export const selectShops = createSelector(selectFeature, (state: ShopState) => {
  state.shops ? state.shops['hydra:member'] : [];
});
export const selectPostCode = createSelector(
  selectFeature,
  (state: ShopState) => state.postCode
);
export const selectMenus = createSelector(selectFeature, (state: ShopState) =>
  state.menus ? state.menus['hydra:member'] : []
);
