import { createAction, props } from '@ngrx/store';

export const resetActiveShop = createAction('[Home] Reset Active Shop');
export const getMerchant = createAction(
  '[Home] Get Merchant',
  props<{
    slug: string;
  }>()
);
export const getMerchantSuccess = createAction(
  '[Home] Get Merchant Success',
  props<{ response: any }>()
);
export const getMerchantFail = createAction(
  '[Home] Get Merchant Fail',
  props<{ error: any }>()
);
export const getShopData = createAction(
  '[Home] Get Shop Data',
  props<{
    slug: string;
    merchantSlug: string;
    postCode?: string;
    serviceTypeId?: number;
    guard?: boolean;
  }>()
);
export const getShopDataSuccess = createAction(
  '[Home] Get Shop Data Success',
  props<{ response: any }>()
);
export const getShopDataFail = createAction(
  '[Home] Get Shop Data Fail',
  props<{ error: any }>()
);
export const getServiceTypes = createAction('[Home] Get Service Types');
export const getServiceTypesSuccess = createAction(
  '[Home] Get Service Types Success',
  props<{ response: any }>()
);
export const getServiceTypesFail = createAction(
  '[Home] Get Service Types Fail',
  props<{ error: any }>()
);
export const changePostCode = createAction(
  '[Home] Change PostCode',
  props<{ postCode: string }>()
);
export const getMenus = createAction(
  '[Order] Get Menus',
  props<{ shopId?: any; shopSlug?: string; guard?: boolean }>()
);
export const getMenusSuccess = createAction(
  '[Order] Get Menus Success',
  props<{ response: any }>()
);
export const getMenusFail = createAction(
  '[Order] Get Menus Fail',
  props<{ error: any }>()
);
