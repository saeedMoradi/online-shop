export class ShopState {
  error: any;
  isLoading: boolean;
  shops: any;
  postCode: string;
  activeShop: any;
  serviceTypes: any;
  menus: any;
  activeMenu: any;
  activeMerchant: any;
}
