import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {
  catchError,
  map,
  mergeMap,
  switchMap,
  exhaustMap
} from 'rxjs/operators';
import { HomeService } from '../services/home.service';
import * as fromActions from './shop.actions';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class ShopEffects {
  constructor(
    private actions$: Actions,
    private shopService: HomeService,
    private ngxService: NgxUiLoaderService,
    private router: Router
  ) {}

  getMerchant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getMerchant),
      switchMap(({ slug }) => {
        return this.shopService.getMerchant(slug).pipe(
          map((data: any) => {
            if (!data['hydra:member'].length) {
              // TODO: redirect 404 merchant
            }
            return fromActions.getMerchantSuccess({
              response: data
            });
          }),
          catchError(error => {
            return of(fromActions.getMerchantFail({ error }));
          })
        );
      })
    )
  );

  getShops$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getShopData),
      switchMap(({ slug, postCode, serviceTypeId, merchantSlug, guard }) => {
        return this.shopService
          .getShops(slug, merchantSlug, postCode, serviceTypeId)
          .pipe(
            mergeMap((data: any) => {
              if (!data['hydra:member'].length) {
                if (!guard) {
                  this.router.navigate(['/404']);
                }
                this.ngxService.stopBackground('get-shops');
                return [fromActions.getShopDataSuccess({ response: data })];
              } else {
                return [
                  fromActions.getShopDataSuccess({ response: data }),
                  fromActions.getMenus({
                    shopId: data['hydra:member'][0].id,
                    shopSlug: data['hydra:member'][0].slug,
                    guard
                  })
                ];
              }
            }),
            catchError(error => {
              this.ngxService.stopBackground('get-shops');
              return of(fromActions.getShopDataFail({ error }));
            })
          );
      })
    )
  );

  getMenus$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getMenus),
      switchMap(({ shopId, shopSlug, guard }) => {
        return this.shopService.getMenus(shopId).pipe(
          map((menus: any) => {
            this.ngxService.stopBackground('get-shops');
            if (!guard) {
              this.router.navigateByUrl(`${this.router.url}${shopSlug}`);
              // this.router.navigate([
              //   [`./${merchant}`, shopSlug],
              //   { relativeTo: this.route }
              // ]);
            }
            // TODO: handle toaster for no menu exist
            // if (menus['hydra:member'].length) {
            // } else {
            // }
            return fromActions.getMenusSuccess({ response: menus });
          }),
          catchError(error => {
            this.ngxService.stopBackground('get-shops');
            return of(fromActions.getMenusFail({ error }));
          })
        );
      })
    )
  );

  getServiceTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getServiceTypes),
      exhaustMap(action => {
        return this.shopService.getServiceTypes().pipe(
          map((data: any) => {
            return fromActions.getServiceTypesSuccess({ response: data });
          }),
          catchError(error => {
            return of(fromActions.getServiceTypesFail({ error }));
          })
        );
      })
    )
  );
}
