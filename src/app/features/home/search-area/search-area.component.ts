import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Store } from '@ngrx/store';
import { changePostCode, getShopData } from '../+store/shop.actions';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { selectIsLoading, selectServiceTypes } from '../+store/shop.selector';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-search-area',
  templateUrl: './search-area.component.html',
  styleUrls: ['./search-area.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchAreaComponent implements OnInit {
  @ViewChild('postCode') postCode: ElementRef;
  active: string = 'Delivery';
  serviceTypes: any;
  isLoading$ = this.store.select(selectIsLoading);
  destroy$ = new Subject();

  constructor(private store: Store, private ngxService: NgxUiLoaderService) {}

  ngOnInit(): void {
    this.store
      .select(selectServiceTypes)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        this.serviceTypes = data;
      });
  }

  ngAfterViewInit() {
    var options = {
      types: ['(regions)'],
      componentRestrictions: { country: 'uk' }
    };
    let autocomplete = new google.maps.places.Autocomplete(
      this.postCode.nativeElement,
      options
    );
    autocomplete.setFields(['address_components']);
    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      for (const component of place.address_components as google.maps.GeocoderAddressComponent[]) {
        const addressType = component.types[0];
        if (addressType === 'postal_code') {
          // TODO: find better way to change input value
          this.postCode.nativeElement.value = component['short_name'];
        }
      }
    });
    // function getPlace() {}
  }

  geolocate() {
    // if (navigator.geolocation) {
    //   navigator.geolocation.getCurrentPosition(function(position) {
    //     var geolocation = {
    //       lat: position.coords.latitude,
    //       lng: position.coords.longitude
    //     };
    //     var circle = new google.maps.Circle(
    //         {center: geolocation, radius: position.coords.accuracy});
    //     autocomplete.setBounds(circle.getBounds());
    //   });
    // }
  }

  changeMode(data: string) {
    this.active = data;
  }

  searchShops() {
    // TODO: check merchant exist or show toaster
    const postCode = this.postCode.nativeElement.value;
    if (postCode.trim()) {
      let type = null;
      if (this.serviceTypes) {
        type = this.serviceTypes.find(element => {
          return element.name.toLowerCase() === this.active.toLowerCase();
        });
      }
      this.store.dispatch(changePostCode({ postCode }));
      this.store.dispatch(
        getShopData({
          slug: null,
          merchantSlug: null,
          postCode,
          serviceTypeId: !!type ? type.id : null
        })
      );
      this.ngxService.startBackground('get-shops');
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
