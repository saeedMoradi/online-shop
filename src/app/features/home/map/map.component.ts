import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { MapInfoWindow, MapMarker, GoogleMap } from '@angular/google-maps';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit {
  @ViewChild(MapInfoWindow) infoWindow: MapInfoWindow;
  @ViewChild('marker') marker: MapMarker;
  @ViewChild('googleMap') googleMap: GoogleMap;
  center = { lat: 51.509865, lng: -0.118092 };
  markerOptions = { draggable: false };
  markerPositions: google.maps.LatLngLiteral[] = [
    { lat: 51.509865, lng: -0.118092 }
  ];
  zoom = 15;
  display?: google.maps.LatLngLiteral;
  options = {
    mapTypeControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    scrollwheel: false
  };
  // circle
  radius = Math.sqrt(20) * 80;
  circleOptions = {
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 1,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  };

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.openInfoWindow(this.marker);
    this.googleMap.panBy(-200, 100);
  }

  addMarker(event: google.maps.MouseEvent) {
    this.markerPositions.push(event.latLng.toJSON());
  }

  move(event: google.maps.MouseEvent) {
    this.display = event.latLng.toJSON();
  }

  openInfoWindow(marker: MapMarker) {
    this.infoWindow.open(marker);
  }

  removeLastMarker() {
    this.markerPositions.pop();
  }
}
