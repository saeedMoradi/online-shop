import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { GoogleMapsModule } from '@angular/google-maps';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeService } from './services/home.service';
import { MapComponent } from './map/map.component';
import { SearchAreaComponent } from './search-area/search-area.component';
import { FoodQualityComponent } from './food-quality/food-quality.component';

import { EffectsModule } from '@ngrx/effects';
import { ShopEffects } from './+store/shop.effects';

@NgModule({
  declarations: [
    HomeComponent,
    MapComponent,
    SearchAreaComponent,
    FoodQualityComponent
  ],
  providers: [HomeService],
  imports: [
    SharedModule,
    HomeRoutingModule,
    GoogleMapsModule,
    EffectsModule.forFeature([ShopEffects])
  ]
})
export class HomeModule {}
