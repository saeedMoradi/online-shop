import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FoodQualityComponent } from './food-quality.component';

describe('FoodQualityComponent', () => {
  let component: FoodQualityComponent;
  let fixture: ComponentFixture<FoodQualityComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodQualityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodQualityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
