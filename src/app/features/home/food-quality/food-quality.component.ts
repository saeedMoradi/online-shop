import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-food-quality',
  templateUrl: './food-quality.component.html',
  styleUrls: ['./food-quality.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FoodQualityComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
