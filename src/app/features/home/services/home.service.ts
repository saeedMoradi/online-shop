import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class HomeService {
  readonly googleApiKey = environment.GOOGLE_API_KEY;
  readonly googleAutoCompleteUrl = environment.GOOGLE_AUTO_COMPLETE_URL;
  private readonly apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getMerchant(slug: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/merchants?slug=${slug}`);
  }

  getShops(
    slug: string,
    merchantSlug: string,
    postCode: string,
    serviceTypeId: number
  ): Observable<any> {
    let url = `${this.apiUrl}/api/shops?`;
    if (slug) {
      url = url + `slug=${slug}&`;
    }
    if (merchantSlug) {
      url = url + `merchant.slug=${merchantSlug}&`;
    }
    if (postCode) {
      url = url + `zipcodeDelivery=${postCode}`;
    }
    // if (serviceTypeId) {
    //   url = url + `serviceType.id=${serviceTypeId}`;
    //   url = url + `serviceType.id=${serviceTypeId}`;
    // }
    
    return this.http.get<any>(url);
  }

  getServiceTypes(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/service_types`);
  }

  getMenus(shopId: string): Observable<any> {
    return this.http.get<any>(
      `${this.apiUrl}/api/menus?page=1&shop.uuid=${shopId}`
    );
  }
}
