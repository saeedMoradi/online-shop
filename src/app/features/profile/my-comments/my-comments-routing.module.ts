import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyCommentsComponent } from './my-comments.component';

const routes: Routes = [{ path: '', component: MyCommentsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyCommentsRoutingModule { }
