import { NgModule } from '@angular/core';

import { MyCommentsRoutingModule } from './my-comments-routing.module';
import { MyCommentsComponent } from './my-comments.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MyCommentsComponent],
  imports: [SharedModule, MyCommentsRoutingModule]
})
export class MyCommentsModule {}
