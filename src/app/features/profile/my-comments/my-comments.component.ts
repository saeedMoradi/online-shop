import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-my-comments',
  templateUrl: './my-comments.component.html',
  styleUrls: ['./my-comments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyCommentsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
