import { NgModule } from '@angular/core';

import { PersonalInfoRoutingModule } from './personal-info-routing.module';
import { PersonalInfoComponent } from './personal-info.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PersonalInfoComponent],
  imports: [SharedModule, PersonalInfoRoutingModule, ReactiveFormsModule]
})
export class PersonalInfoModule {}
