import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginResponse } from 'src/app/shared/interfaces';
import { editUser, login } from '../../auth/+store/auth.actions';
import { AuthFacade } from '../../auth/+store/auth.facade';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalInfoComponent implements OnInit {
  userForm: FormGroup;
  profile: LoginResponse;

  constructor(
    private formBuilder: FormBuilder,
    public authFacade: AuthFacade,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]]
    });
    this.authFacade.profile$.subscribe((data: LoginResponse) => {
      if (data) {
        this.profile = data;
        this.userForm.patchValue({
          email: data.email,
          firstName: data.firstName,
          lastName: data.lastName
        });
        this.cd.detectChanges();
      }
    });
  }

  get f() {
    return this.userForm.controls;
  }

  onEdit() {
    this.userForm.markAllAsTouched();
    if (this.userForm.invalid) {
      return;
    }
    this.authFacade.dispatch(
      editUser({ id: this.profile.id, request: this.userForm.value })
    );
  }

  ngOnDestroy() {}
}
