import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AuthFacade } from '../auth/+store/auth.facade';
import { logout } from '../auth/+store/auth.actions';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit {
  constructor(public authFacade: AuthFacade) {}

  ngOnInit(): void {}

  logout() {
    this.authFacade.dispatch(logout());
  }
}
