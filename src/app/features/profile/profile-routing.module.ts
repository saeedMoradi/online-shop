import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      { path: '', redirectTo: 'personal-info' },
      {
        path: 'my-orders',
        loadChildren: () =>
          import('./my-orders/my-orders.module').then(m => m.MyOrdersModule)
      },
      {
        path: 'my-comments',
        loadChildren: () =>
          import('./my-comments/my-comments.module').then(
            m => m.MyCommentsModule
          )
      },
      {
        path: 'personal-info',
        loadChildren: () =>
          import('./personal-info/personal-info.module').then(
            m => m.PersonalInfoModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}
