import { NgModule } from '@angular/core';

import { MyOrdersRoutingModule } from './my-orders-routing.module';
import { MyOrdersComponent } from './my-orders.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MyOrdersComponent],
  imports: [SharedModule, MyOrdersRoutingModule]
})
export class MyOrdersModule {}
