import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap, switchMap } from 'rxjs/operators';
import * as authActions from './auth.actions';
import { AuthService } from '../services/auth.service';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import {
  ForgotRequest,
  ForgotResponse,
  LoginRequest,
  LoginResponse,
  RegisterRequest,
  RegisterResponse,
  VerifyRequest,
  VerifyResponse
} from 'src/app/shared/interfaces';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private persistanceService: PersistanceService,
    private router: Router
  ) {}

  checkAuth$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.checkAuth),
      map(() => {
        const isAuthenticated: LoginResponse = this.authService.checkAuth();
        if (isAuthenticated) {
          return authActions.checkAuthSuccess({ profile: isAuthenticated });
        } else {
          return authActions.checkAuthFail();
        }
      })
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.login),
      map(action => {
        return action.request;
      }),
      exhaustMap((requestData: LoginRequest) => {
        return this.authService.login(requestData).pipe(
          map((profile: LoginResponse) => {
            this.persistanceService.set('profile', profile);
            return authActions.loginSuccess({ profile });
          }),
          catchError(error => of(authActions.loginFail({ error })))
        );
      })
    )
  );

  OAuthGoogle$ = createEffect(() => 
    this.actions$.pipe(
      ofType(authActions.OAuthGoogle),
      switchMap(action => {
        return this.authService
          .OAuthGoogle(action.authToken, action.idToken)
          .pipe(
            map((profile: any) => {
              this.persistanceService.set('profile', profile);
              return authActions.loginSuccess({ profile });
            }),
            catchError(error => {
              return of(authActions.loginFail({ error }));
            })
          );
      })
    )
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.register),
      map(action => {
        return action.request;
      }),
      switchMap((action: RegisterRequest) => {
        return this.authService.register(action).pipe(
          map((profile: RegisterResponse) => {
            return authActions.registerSuccess({ profile });
          }),
          catchError(error => of(authActions.registerFail({ error })))
        );
      })
    )
  );

  editUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.editUser),
      switchMap(({ id, request }) => {
        return this.authService.editUser(id, request).pipe(
          map((data: any) => {
            const profile = this.persistanceService.get('profile');
            this.persistanceService.set('profile', {
              ...profile,
              email: data.email,
              firstName: data.firstName,
              lastName: data.lastName
            });
            return authActions.editUserSuccess({ profile: data });
          }),
          catchError(error => {
            return of(authActions.editUserFail({ error }));
          })
        );
      })
    )
  );

  forgot$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.forgotPassword),
      map(action => {
        return action.request;
      }),
      switchMap((action: ForgotRequest) => {
        return this.authService.forgotPassword(action).pipe(
          map((data: ForgotResponse) => {
            return authActions.forgotPasswordSuccess({ data });
          }),
          catchError(error => of(authActions.forgotPasswordFail({ error })))
        );
      })
    )
  );

  verify$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.verifyPassword),
      map(action => {
        return action.request;
      }),
      switchMap((action: VerifyRequest) => {
        return this.authService.verifyPassword(action).pipe(
          map((data: VerifyResponse) => {
            return authActions.verifyPasswordSuccess({ data });
          }),
          catchError(error => of(authActions.verifyPasswordFail({ error })))
        );
      })
    )
  );

  getUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.getUsers),
      switchMap((action: any) => {
        return this.authService.getUsers().pipe(
          map((data: any) => {
            return authActions.getUsersSuccess({ response: data });
          }),
          catchError(error => of(authActions.getUsersFail({ error })))
        );
      })
    )
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.logout),
      switchMap(() => {
        this.persistanceService.remove('profile');
        return of(authActions.logoutSuccess());
      })
    )
  );

  redirectToHome$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.loginSuccess),
        tap(() => {
          this.router.navigate(['/home']);
        })
      ),
    { dispatch: false }
  );

  redirectToLogin$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          authActions.registerSuccess,
          authActions.logoutSuccess,
          authActions.registerSuccess
        ),
        tap(() => {
          this.router.navigate(['/login']);
        })
      ),
    { dispatch: false }
  );

  redirectToReset$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.forgotPasswordSuccess),
        tap(() => {
          this.router.navigate(['/reset-password']);
        })
      ),
    { dispatch: false }
  );
}
