import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {
  LoginRequest,
  RegisterRequest,
  LoginResponse,
  RegisterResponse,
  ForgotRequest,
  ForgotResponse,
  VerifyRequest,
  VerifyResponse,
} from 'src/app/shared/interfaces';
import { PersistanceService } from 'src/app/core/services/persistance.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  login(data: LoginRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(
      `${this.apiUrl}/authentication_token`,
      data
    );
  }

  OAuthGoogle(authToken?: string, idToken?: string): Observable<any> {
    return this.http.post(`${this.apiUrl}/connect/google/check`, {authToken, idToken});
  }

  register(data: RegisterRequest): Observable<RegisterResponse> {
    return this.http.post<RegisterResponse>(`${this.apiUrl}/api/users`, data);
  }

  getUsers(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/users`);
  }

  editUser(id, data): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/users/${id}`, data);
  }

  forgotPassword(data: ForgotRequest): Observable<ForgotResponse> {
    return this.http.post<ForgotResponse>(
      `${this.apiUrl}/api/users/forgetpassword`,
      data
    );
  }

  verifyPassword(data: VerifyRequest): Observable<VerifyResponse> {
    return this.http.post<VerifyResponse>(
      `${this.apiUrl}/api/users/verifypassword`,
      data
    );
  }

  checkAuth() {
    const profile = this.persistanceService.get('profile');
    return profile ? profile : null;
  }
}
