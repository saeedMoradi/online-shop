import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';

import { AuthService } from './auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AuthService', () => {
  let spectator: SpectatorService<AuthService>;
  const createService = createServiceFactory({
    service: AuthService,
    imports: [HttpClientTestingModule]
  });

  beforeEach(() => (spectator = createService()));

  it('should not be logged in', () => {
    expect(spectator.service.checkAuth).toBeTruthy();
  });
});
