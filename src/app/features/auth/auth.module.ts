import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromAuthReducer from './+store/auth.reducer';
import { AuthEffects } from './+store/auth.effects';
//Interceptors
import { HttpErrorInterceptor } from './interceptors/error.interceptor';
import { ContentTypeInterceptor } from './interceptors/content-type.interceptor';
import { AuthorizationInterceptor } from './interceptors/authorization.interceptor';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAuthReducer.featureKey, fromAuthReducer.reducer),
    EffectsModule.forFeature([AuthEffects])
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ContentTypeInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  declarations: []
})
export class AuthModule {}
