import { NgModule } from '@angular/core';
import { ReviewsRoutingModule } from './reviews-routing.module';
import { ReviewsComponent } from './reviews.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ReviewsComponent],
  imports: [SharedModule, ReviewsRoutingModule]
})
export class ReviewsModule {}
