import { NgModule } from '@angular/core';

import { ContactUsRoutingModule } from './contact-us-routing.module';
import { ContactUsComponent } from './contact-us.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ContactUsComponent],
  imports: [SharedModule, ContactUsRoutingModule]
})
export class ContactUsModule {}
