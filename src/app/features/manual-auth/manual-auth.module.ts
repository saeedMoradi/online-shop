import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ManualAuthRoutingModule } from './manual-auth-routing.module';
import { ManualAuthComponent } from './manual-auth.component';

@NgModule({
  declarations: [ManualAuthComponent],
  imports: [SharedModule, ManualAuthRoutingModule],
})
export class ManualAuthModule {}
