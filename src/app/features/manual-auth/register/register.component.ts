import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OAuthGoogle, register } from '../../auth/+store/auth.actions';
import { AuthFacade } from '../../auth/+store/auth.facade';
// Loggin By Social Media
import { SocialAuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  user: SocialUser;
  googleLoginProvider = GoogleLoginProvider;

  constructor(
    private formBuilder: FormBuilder,
    public authFacade: AuthFacade,
    private authService: SocialAuthService,
  ) {}

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onRegister() {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.invalid) {
      return;
    }
    this.authFacade.dispatch(register({ request: this.registerForm.value }));
  }

  OAuthGoogle() {
    this.authService.authState.subscribe(user => {
      this.user = user;
      this.authFacade.dispatch(OAuthGoogle({authToken: this.user['authToken'], idToken: this.user['idToken']}));
    });
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  ngOnDestroy() {}
}
