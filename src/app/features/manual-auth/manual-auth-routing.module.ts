import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManualAuthComponent } from './manual-auth.component';

const routes: Routes = [
  {
    path: '',
    component: ManualAuthComponent,
    children: [
      { path: '', redirectTo: 'login' },
      {
        path: 'login',
        loadChildren: () =>
          import('./login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'register',
        loadChildren: () =>
          import('./register/register.module').then(m => m.RegisterModule)
      }
    ]
  },
  {
    path: 'forgot',
    loadChildren: () =>
      import('./forgot/forgot.module').then(m => m.ForgotModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManualAuthRoutingModule {}
