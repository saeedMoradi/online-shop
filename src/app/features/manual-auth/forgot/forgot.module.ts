import { NgModule } from '@angular/core';

import { ForgotRoutingModule } from './forgot-routing.module';
import { ForgotComponent } from './forgot.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ForgotComponent],
  imports: [SharedModule, ForgotRoutingModule, ReactiveFormsModule]
})
export class ForgotModule {}
