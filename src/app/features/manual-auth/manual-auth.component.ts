import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-manual-auth',
  templateUrl: './manual-auth.component.html',
  styleUrls: ['./manual-auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManualAuthComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
