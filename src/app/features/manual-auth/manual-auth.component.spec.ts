import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ManualAuthComponent } from './manual-auth.component';

describe('ManualAuthComponent', () => {
  let component: ManualAuthComponent;
  let fixture: ComponentFixture<ManualAuthComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
