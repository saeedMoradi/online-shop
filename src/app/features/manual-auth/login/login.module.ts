import { NgModule } from '@angular/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
// Loggin By Social Media
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

@NgModule({
  declarations: [LoginComponent],
  imports: [SharedModule, LoginRoutingModule, ReactiveFormsModule, SocialLoginModule],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: true,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '8645829638-51qvekr1fdv8i27f68sdbopdri2oav6v.apps.googleusercontent.com'
          )
        }
      ]
    } as SocialAuthServiceConfig
  }],
})
export class LoginModule {}
