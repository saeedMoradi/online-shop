import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { login, OAuthGoogle } from '../../auth/+store/auth.actions';
import { AuthFacade } from '../../auth/+store/auth.facade';
// Loggin By Social Media
import { SocialAuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  user: SocialUser;
  googleLoginProvider = GoogleLoginProvider;

  constructor(
    private formBuilder: FormBuilder,
    public authFacade: AuthFacade,
    private authService: SocialAuthService,
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onLogin() {
    this.loginForm.markAllAsTouched();
    if (this.loginForm.invalid) {
      return;
    }
    this.authFacade.dispatch(login({ request: this.loginForm.value }));
  }

  OAuthGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(userData => {
      this.user = userData;
      this.authFacade.dispatch(OAuthGoogle({authToken: this.user['authToken'], idToken: this.user['idToken']}));
    }).catch(error=> console.log('my-error',error));
  }
  
  signOut(): void {
    this.authService.signOut();
  }

  ngOnDestroy() {}
}
