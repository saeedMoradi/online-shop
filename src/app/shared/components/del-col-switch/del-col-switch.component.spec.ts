import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DelColSwitchComponent } from './del-col-switch.component';

describe('DelColSwitchComponent', () => {
  let component: DelColSwitchComponent;
  let fixture: ComponentFixture<DelColSwitchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DelColSwitchComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelColSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
