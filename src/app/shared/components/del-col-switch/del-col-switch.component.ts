import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs'
import { selectActiveShop } from '../../../features/home/+store/shop.selector';

@Component({
  selector: 'app-del-col-switch',
  templateUrl: './del-col-switch.component.html',
  styleUrls: ['./del-col-switch.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DelColSwitchComponent implements OnInit {
  _mode: boolean = false;
  serviceActive$: Observable<any> = this.store.select(selectActiveShop);  
  showService: boolean = true;
  showSlide: boolean = true;
  singleService: string;


  @Input('mode') set mode(value: string) {
    if (value === 'Delivery') {
      this._mode = false;
    } else {
      this._mode = true;
    }
  }
  @Output() onChangeMode = new EventEmitter<string>();

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.serviceActive$.subscribe((active: any) => {
      if (active) {
        switch (active.serviceType.length) {
          case 0:
            this.showService = !this.showService
            break;
          case 1:
            this.singleService = active.serviceType[0].name;
            this.showSlide = !this.showSlide;
            break;
        }    

      }
    })
  }

  switchMode($event) {
    this._mode = !this._mode;
    if (this._mode) {
      this.onChangeMode.emit('Collection');
    } else {
      this.onChangeMode.emit('Delivery');
    }
  }
}
