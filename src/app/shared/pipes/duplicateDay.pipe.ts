import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'duplicateDays' })
export class duplicateDays implements PipeTransform {
  transform(workTimes, args?: any): any {
    let obj = {};
    workTimes.forEach(data => {
      const open = data.open;
      const close = data.close;
      data.days.forEach(element => {
        if (obj[element.name]) {
          obj[element.name] = [...obj[element.name], { open, close }];
        } else {
          obj[element.name] = [{ open, close }];
        }
      });
    });
    // console.log(obj)
    return obj;
  }
}
