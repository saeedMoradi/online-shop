import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageUrl'
})
export class ImageUrlPipe implements PipeTransform {
  transform(value: string) {
    if (value) {
      return value;
    }
    return 'assets/images/image-placeholder-2.png';
  }
}
