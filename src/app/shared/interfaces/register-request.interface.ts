export interface RegisterRequest {
  restaurantName: string;
  firstname: string;
  lastname: string;
  email: string;
  password: string;
}
