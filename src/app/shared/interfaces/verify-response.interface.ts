export interface VerifyResponse {
  id: number;
  email: string;
  roles: string[];
  restaurantName: string;
  firstName: string;
  lastName: string;
}
