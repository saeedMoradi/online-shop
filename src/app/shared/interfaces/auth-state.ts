import { CurrentUser } from './current-user';

export interface AuthState {
  isLoading: boolean;
  isLoggedIn: boolean;
  currentUser: CurrentUser | null;
  error: string | null;
}
