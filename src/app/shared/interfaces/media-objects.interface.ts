export interface MediaObjects {
  '@context': string;
  '@id': string;
  '@type': string;
  contentUrl: string;
}
