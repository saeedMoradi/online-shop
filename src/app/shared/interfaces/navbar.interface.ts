export interface Link {
  routeName: string;
  routePath: any[];
  iconName: string;
  children: Children[];
}
export interface Children {
  routeName: string;
  routePath: any[];
}
