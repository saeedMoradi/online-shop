export interface Shop {
  id: number;
  createdAt: string;
  updatedAt: string;
  title: string;
  zipCode: string;
  location: Location;
  merchant: string;
  timeZone: string;
  city: string;
  country: string;
  managerName: string;
  streetNameNumber: string;
  currency: string;
  url?: any;
  managers: any[];
  mealDeal: any[];
  pickups: any[];
  preOrders: any[];
  deliveries: any[];
  worktimes: Worktime[];
  buildingNumber: string;
  address: string;
  state: string;
  email: string;
  phone: string;
}

export interface Worktime {
  id: number;
  day: string;
  Shop: string;
  times?: Time[];
}

export interface Time {
  open: Open;
  close: Open;
}

export interface Open {
  date: string;
  timezone: string;
  timezone_type: number;
}

export interface Location {
  type: string;
  coordinates: number[];
  srid?: any;
}
