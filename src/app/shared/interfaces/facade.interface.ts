export interface Facade {
  dispatch(action): void;
}
