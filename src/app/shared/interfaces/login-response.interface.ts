export interface LoginResponse {
  token: string;
  id: number;
  firstName?: any;
  lastName?: any;
  username: string;
  fullName: string;
  email: string;
  restaurantName?: any;
  roles: string[];
}
