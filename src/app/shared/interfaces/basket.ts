export interface Basket {
  product: any;
  quantity: number;
  total: number;
  selectedAccessories: any[];
  selectedCombinations: any[];
  selectedIngredients: any[];
}
