import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
// Components
import { DelColSwitchComponent } from './components/del-col-switch/del-col-switch.component';
// Pipes
import { TruncatePipe } from './pipes/truncate.pipe';
import { ImageUrlPipe } from './pipes/image-url.pipe';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { duplicateDays } from './pipes/duplicateDay.pipe';

@NgModule({
  declarations: [
    DelColSwitchComponent,
    TruncatePipe,
    ImageUrlPipe,
    CapitalizePipe,
    duplicateDays,
  ],
  imports: [CommonModule, NgxUiLoaderModule],
  exports: [
    CommonModule,
    NgxUiLoaderModule,
    DelColSwitchComponent,
    TruncatePipe,
    ImageUrlPipe,
    CapitalizePipe,
    duplicateDays,
  ]
})
export class SharedModule {}
